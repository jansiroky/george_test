import {fireEvent, render, screen} from '@testing-library/react';
import App from "./App";
import React from "react";

test('search input logic & validation', () => {
  render(<App/>);

  const input = screen.getByPlaceholderText('Search');

  expect(input).toBeInTheDocument();

  fireEvent.change(input, {target: {value: 'slovakia123'}});

  expect(input).toHaveValue('slovakia');

  expect(window.location.hash).toEqual('#/?search=slovakia');
});