import {CurrencyData} from "../interfaces";

export class CurrencyRatesService {
  static API_URL: string = 'https://run.mocky.io/v3/c88db14a-3128-4fbd-af74-1371c5bb0343';

  static async get(): Promise<CurrencyData[]> {
    try {
      return (await (await fetch(CurrencyRatesService.API_URL)).json())['fx'].filter(this.validateRate);
    } catch (error) {
      throw error;
    }
  }

  static validateRate(rate: CurrencyData): boolean {
    return rate.exchangeRate != null && rate.nameI18N != null;
  }
}