import {CurrencyRatesService} from "./CurrencyRatesService";

test('currency rates validation', () => {
  const invalidRate: any = {
    "currency": "STD",
    "precision": 2
  };

  expect(CurrencyRatesService.validateRate(invalidRate)).toEqual(false);

  const invalidRate2: any =
    {
      "currency": "LVL",
      "precision": 2,
      "denominations": [
        500,
        100,
        50,
        20,
        10,
        5
      ],
      "exchangeRate": {
        "buy": 0.6821,
        "middle": 0.6996,
        "sell": 0.7171,
        "indicator": 0,
        "lastModified": "2013-12-30T23:00:00Z"
      },
      "banknoteRate": {
        "buy": 0.685304,
        "middle": 0.702804,
        "sell": 0.720304,
        "indicator": 0,
        "lastModified": "2013-12-30T23:00:00Z"
      },
      "flags": [
        "provided"
      ]
    };

  expect(CurrencyRatesService.validateRate(invalidRate2)).toEqual(false);

  const validRate: any = {
    "currency": "SCR",
    "precision": 2,
    "nameI18N": "Seychelles-Rupee",
    "exchangeRate": {
      "buy": 14.7246,
      "middle": 15.4746,
      "sell": 16.2246,
      "indicator": 0,
      "lastModified": "2018-11-08T23:00:00Z"
    },
    "banknoteRate": {
      "buy": 14.167,
      "middle": 15.667,
      "sell": 17.167,
      "indicator": 0,
      "lastModified": "2018-11-06T23:00:00Z"
    },
    "flags": [
      "provided"
    ]
  };

  expect(CurrencyRatesService.validateRate(validRate)).toEqual(true);
});


test('currency rates API', async () => {
  const rates = await CurrencyRatesService.get();

  expect(rates.length).toBeGreaterThan(0);

  expect(rates).toContainEqual(expect.objectContaining({
    exchangeRate: expect.objectContaining({
      middle: expect.any(Number),
    }),
    nameI18N: expect.any(String),
  }));
});