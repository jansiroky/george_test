import {CountriesService} from "./CountriesService";
import {CountryData} from "../interfaces";

test('countries filter', async () => {
  const countryData: CountryData[] = await CountriesService.get();

  expect(CountriesService.filter(countryData, 'hun')).toContainEqual(
    expect.objectContaining({
      countryName: 'Hungary',
    })
  );

  expect(CountriesService.filter(countryData, 'huf')).toContainEqual(
    expect.objectContaining({
      currencySymbol: 'HUF',
    })
  );

  expect(CountriesService.filter(countryData, 'slovakia')).toContainEqual(
    expect.objectContaining({
      countryName: 'Slovakia',
      exchangeRate: 1,
    })
  );
});


test('countries get data logic', async () => {
  const rates = await CountriesService.get();

  expect(rates.length).toBeGreaterThan(0);

  expect(rates).toContainEqual(expect.objectContaining({
    isoCode: expect.any(String),
    countryName: expect.any(String),
    currencySymbol: expect.any(String),
    currencyName: expect.any(String),
    exchangeRate: expect.any(Number),
  }));
});