import {CountryData, CurrencyData} from "../interfaces";
import {CurrencyRatesService} from "./CurrencyRatesService";
import {countriesData} from "./CountriesData";

export class CountriesService {
  static async get(): Promise<CountryData[]> {
    const currencyRates: CurrencyData[] = await CurrencyRatesService.get();

    const countries: CountryData[] = [];

    Object.entries(countriesData).forEach(([id, {name, currency_symbol}]) => {
      const currencyRate = currencyRates.find((rate) => rate.currency === currency_symbol);

      if (currencyRate != null) {
        countries.push({
          isoCode: id,
          countryName: name,
          currencySymbol: currency_symbol,
          currencyName: currencyRate.nameI18N,
          exchangeRate: currencyRate.exchangeRate?.middle ?? 0,
        })
      }
    });

    return countries;
  }

  static filter = (data: CountryData[], searchPhrase = ''): CountryData[] => {
    return data?.filter((currencyRate) => {
      return currencyRate.currencySymbol?.toLowerCase().startsWith(searchPhrase.trim().toLowerCase())
        || currencyRate.currencyName?.toLowerCase().includes(searchPhrase.trim().toLowerCase())
        || currencyRate.countryName?.toLowerCase().includes(searchPhrase.trim().toLowerCase());
    });
  }
}