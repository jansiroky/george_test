export interface CurrencyData {
  currency: string;
  precision: number;
  nameI18N: string;
  exchangeRate: Rate;
  banknoteRate: Rate;
  flags: string[];
  denominations: number[];
}

interface Rate {
  buy: number;
  middle: number;
  sell: number;
  indicator: number;
  lastModified: string;
}

export interface CountryData {
  isoCode: string;
  countryName: string;
  currencySymbol: string;
  currencyName: string;
  exchangeRate: number;
}