import React from 'react';
import {render, screen} from '@testing-library/react';
import App from './App';


it('renders title', () => {
  render(<App/>);

  expect(screen.getByText('George FE Test')).toBeInTheDocument();
})


it('renders search input', () => {
  render(<App/>);

  expect(screen.getByPlaceholderText('Search')).toBeInTheDocument();
})


it('renders loading text', () => {
  render(<App/>);

  expect(screen.getByText('Loading rates...')).toBeInTheDocument();
})
