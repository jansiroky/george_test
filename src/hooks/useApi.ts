import {useCallback, useState} from "react";

export function useApi<T>(apiFunction: Function): [
  UseApiState<T>,
  Function
] {
  const [response, setResponse] = useState<UseApiState<T>>({
    data: null,
    isSuccess: false,
    isLoading: false,
    hasErrors: false,
  });

  const callApi = useCallback(async () => {
    try {
      setResponse(prevState => ({
        ...prevState,
        isLoading: true,
      }));

      const apiData = await apiFunction();

      setResponse({
        data: apiData,
        isSuccess: true,
        isLoading: false,
        hasErrors: false,
      });
    } catch (error) {
      console.error(error);

      setResponse({
        data: null,
        isSuccess: false,
        isLoading: false,
        hasErrors: true,
      });
    }
  }, [apiFunction]);

  return [response, callApi];
}

interface UseApiState<DataType> {
  data: DataType | null;
  isSuccess: boolean;
  isLoading: boolean;
  hasErrors: boolean;
}