import React, {ReactElement} from 'react';
import './App.css';
import Footer from "./components/Footer";
import Menu from "./components/Menu";
import {HashRouter, Route, Switch} from "react-router-dom";
import Home from './Home';

function App(): ReactElement {

  return (
    <HashRouter
      basename="/"
    >
      <Menu/>
      <Switch>
        <Route path="/">
          <Home/>
        </Route>
      </Switch>
      <Footer/>
    </HashRouter>
  );
}

export default App;
