import {ReactElement} from "react";
import {CountryData} from "../interfaces";
import styles from './CurrencyCard.module.css';

function CurrencyCard({data}: Props): ReactElement {
  return (
    <div className="flex flex-col md:flex-row content-center justify-center my-2 border shadow-md rounded p-3">
      <div>
        <img className={styles.currencyCard__flag}
             src={`flags/${data.isoCode.toLowerCase()}.png`}
             alt={data.countryName}/>
      </div>
      <div className="flex-1 flex flex-row">
        <span className="flex-1">{data.countryName}</span>
        <span className="flex-1 text-right md:text-left">{data.currencySymbol}</span>
      </div>
      <div className="flex-1 flex flex-row">
        <span className="flex-1">{data.currencyName}</span>
        <span className="flex-1 text-right">{data.exchangeRate} EUR</span>
      </div>
    </div>
  )
}

export default CurrencyCard;


interface Props {
  data: CountryData;
}