import React from 'react';
import {render, screen} from '@testing-library/react';
import CurrencyCard from './CurrencyCard';
import {CountryData} from "../interfaces";


it('renders currency card content', () => {
  const countryData: CountryData = {
    currencyName: 'Euro',
    exchangeRate: 1,
    countryName: 'Slovakia',
    currencySymbol: 'EUR',
    isoCode: 'sk',
  }

  render(<CurrencyCard data={countryData}/>);

  const displayedImage = document.querySelector("img") as HTMLImageElement;
  expect(displayedImage.src).toContain("sk.png");
  expect(screen.getByText('Slovakia')).toBeInTheDocument();
  expect(screen.getByText('EUR')).toBeInTheDocument();
  expect(screen.getByText('Euro')).toBeInTheDocument();
  expect(screen.getByText('1 EUR')).toBeInTheDocument();
})
