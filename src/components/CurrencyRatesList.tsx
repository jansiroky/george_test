import React, {ReactElement, useEffect} from "react";
import CurrencyCard from "./CurrencyCard";
import {useApi} from "../hooks/useApi";
import {CountryData} from "../interfaces";
import {CountriesService} from "../api/CountriesService";


function CurrencyRatesList({searchPhrase}: Props): ReactElement {
  const [countriesResponse, getCountries] = useApi<CountryData[]>(CountriesService.get);

  useEffect(() => {
    getCountries();
  }, [getCountries]);

  const {data: rateData, isSuccess, isLoading, hasErrors} = countriesResponse;

  if (isLoading || hasErrors) {
    return <div className="mt-16 text-center">
      {isLoading && <span>Loading rates...</span>}
      {hasErrors && <span>There was an error loading rates. Please try again later.</span>}
    </div>;
  }

  if (isSuccess) {
    const filteredCountries: CountryData[] = CountriesService.filter(rateData!, searchPhrase);

    return (
      <>
        {filteredCountries.map((countryData) =>
          <CurrencyCard key={countryData.isoCode} data={countryData}/>
        )}
      </>
    );
  }

  return <div/>;
}

export default CurrencyRatesList;

interface Props {
  searchPhrase: string;
}