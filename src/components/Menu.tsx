import React, {ReactElement} from "react";
import {Link} from "react-router-dom";

function Menu(): ReactElement {
  return (
    <nav className="bg-blue-900">
      <div className="container mx-auto px-4 md:px-0">
        <div className="relative flex items-center justify-between h-16">
          <Link to="/">
            <h1 className="text-white font-semibold">George FE Test</h1>
          </Link>
        </div>
      </div>
    </nav>
  )
}

export default Menu;