import {ReactElement} from "react";

function Footer(): ReactElement {
  return (
    <footer className="my-6 bg-accent-7 text-center text-gray-500 text-sm">
      &copy; 2021 Ján Široký
    </footer>
  )
}

export default Footer;