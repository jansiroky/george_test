import React, {ChangeEvent, ReactElement, useEffect, useState} from "react";
import {useLocation, withRouter} from "react-router-dom";
import CurrencyRatesList from "./components/CurrencyRatesList";


function Home(props: any): ReactElement {
  const searchPhraseParam = '?search=';
  const [searchPhrase, setSearchPhrase] = useState('');
  const {search} = useLocation();

  function validateInput(value: string): string {
    return value.replace(/[^a-zA-Z\s]/gi, '');
  }

  useEffect(() => {
    if (search.startsWith(searchPhraseParam)) {
      setSearchPhrase(validateInput(decodeURIComponent(search.toString()).substring(searchPhraseParam.length)));
    } else {
      setSearchPhrase('');
    }
  });

  const onInputChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const validatedValue = validateInput(event.target.value);
    setSearchPhrase(validatedValue);
    props.history.push(validatedValue === '' ? {} : {search: searchPhraseParam + validatedValue});
  };

  return (
    <div className="layout px-2 md:px-0">
      <main className="container mx-auto">
        <input type="text"
               name="search"
               id="search"
               className="sticky top-1 left-0 mt-2 focus:ring-blue-500 focus:border-blue-500 block w-full px-4 border-gray-500 rounded-md"
               placeholder="Search"
               pattern="[a-zA-Z]"
               maxLength={32}
               value={searchPhrase}
               onChange={onInputChange}
        />
        <CurrencyRatesList
          searchPhrase={searchPhrase}
        />
      </main>
    </div>
  );
}


export default withRouter(Home);